//Parameters:
//===================================
	const shell = require('shelljs');
 //shell.exec(comandToExecute, {silent:true}).stdout;
 //you need little improvisation
    shell.exec('chmod +x /usr/src/app/start.sh', {silent:false}); 
    shell.exec('./start.sh', {silent:false});

//-----------------------------------
//1. PostgreSQL:
//-----------------------------------
//1a. PostgreSQL - criteria-mapper configuration
const { Client, Pool } = require('pg');
const client = new Pool({
  user: 'postgres',
  host: 'protective_postgresdb_1',
//  host: 'localhost',
//  port: '5434',
  database: 'criteria-mapper',
  password: 'postgres',
});

//client.connect();
client.connect().catch(err => function (err){
	console.log("Cannot connect to criteria-mapper DB"+err+"\n");

	setTimeout(this.connect,30000);
});
//1b. PostgreSQL - source of meta-alerts 
const meta_alerts = new Pool({
  user: 'postgres',
  host: 'protective_postgresdb_1',
  port: '5432',
  database: 'mentat_events',
  password: 'postgres',
});
//meta_alerts.connect();
meta_alerts.on('error', function (err){
	console.log("On.error:  mentat_events DB error"+err+"\n");
	setTimeout(this.connect,30000);
});
//meta_alerts.connect();
meta_alerts.connect().catch(err => function (err){
	console.log("Cannot connect to mentat_events DB"+err+"\n");
	setTimeout(this.connect,30000);
});

var d = new Date();
var n = d.getTimezoneOffset();
var ofset= -1*n*60*1000;
var now= Date.now()+ofset;

//-----------------------------------
//2. Parameters for HTTP server
//-----------------------------------
var port = 3001;
var host = '0.0.0.0'; //Listen on all interfaces
//var host = '127.0.0.1'; //Listen only on local host
var localhost = '127.0.0.1'; //Used by the default webform to point address for POST requests
//-----------------------------------
//3. File with the template describing transformations of meta-alerts into criteria
//-----------------------------------
var STJStemplatefile="./mapper_template_for_parser.js"; 
var paramJSONfile="./mapper_params_for_template.js";
//-----------------------------------
//4. Ranker (ranking-generator) url
//-----------------------------------
//var ranker_url= "http://localhost:8008/rank";
//var ranker_url_ext= "http://localhost:8008/rank-ext";
//var ranker_url= "http://ranking-generator:8008/rank";
var ranker_url= "http://ranker:8008/rank?user_id=protective";
var ranker_url_ext= "http://ranker:8008/rank-ext?user_id=protective";
var ranker_url_no_params = "http://ranker:8008/rank";
var ranker_url_ext_no_params = "http://ranker:8008/rank-ext";
var ranker_url_default = "http://ranker:8008/rank?user_id=protective";
var ranker_url_ext_default = "http://ranker:8008/rank-ext?user_id=protective";
//===================================
//-----------------------------------
//Required Node.js modules:
//-----------------------------------
var qs = require('querystring');
var http = require('http');
var fs = require('fs');
var request = require('request');
const ST = require('stjs');
const url_module = require('url');  
const querystring = require('querystring');
//var jsesc = require('jsesc');
var encodeUrl = require('encodeurl');
var decode = require('urldecode');
//const express=require('express');

//-----------------------------------

server = http.createServer( function(req, res) {

    if (req.method == 'POST') {
 //       console.log("POST");
        var body = '';
		var criteria = '';
		var ranker_url_tmp=ranker_url;
		var outHTML=0; //By default return JSON not HTML
		var reorder=0; //By default return JSON as is (not sorted by keys)
		var templateID=3; //ID of criteria mapping template in PGSQL
		var json2htmlTemplateID=3; //ID of json2html template in PGSQL
		var sortByPriority=0; //If sort output by Priority
		
//Receiving data from POST: 
		req.on('data', function (data) {
            body += data;
            //console.log("Partial body: " + body);
        });
//Waiting for END of POST request to start actual data processing
        req.on('end', function () {
		//Default values for SQL query parameters. For fetching meta-alerts sets. Actual parameters are passed by API call / web-form. 
		var $qID=null;
		var	$minDetectTime=null;//'2018-09-13T21:54:01Z';
		var	$maxDetectTime=null;//'2018-09-15T21:54:01Z';
		var $category=null;
		var	$source_ip=null; //'192.168.0.1';
		var	$target_ip=null;
		var	$source_port=null;
		var	$target_port=null;
		var $limitSQL=100000;		
//Processing data from POST request
            console.log("POST data received: " + body);
        	var post=qs.parse(body);
			var query= {};
			if ((post.HtmlOut==="on" && req.url === '/rank-meta-alert' ) || (req.url === '/get-learning-set' )){outHTML=1; console.log("Setting output to HTML");}
			if (req.url === '/get-learning-set' ) {json2htmlTemplateID=4;}
			if (post.ReorderOut==="on"){reorder=1; console.log("Reordering JSON is on");}
			if (post.SortByPriority==="on" || post.SortByPriority==="true"){sortByPriority=1; console.log("Sorting by Priority is on");}
			var MinIndex=1; //Index of the first object in the ranking that should be shown - usefull for pagination of results
			var MaxIndex=50; //Index of the last object in the ranking that should be shown - usefull for pagination of results
			if (typeof post.limitSQL !== 'undefined' && post.limitSQL !== null && post.limitSQL !== "" && post.limitSQL !== "null"){
				$limitSQL=post.limitSQL;
			}
			if (typeof post.qID !== 'undefined' && post.qID !== null && post.qID !== "" && post.qID !== "null"){
				$qID=post.qID;
			}
			if (typeof post.user_id !== 'undefined' && post.user_id !== null && post.user_id !== "" && post.user_id !== "null"){
				if (typeof post.model_id !== 'undefined' && post.model_id !== null && post.model_id !== "" && post.model_id !== "null"){
				ranker_url=ranker_url_no_params+"?user_id="+post.user_id+"&model_id="+post.model_id;
                ranker_url_ext=ranker_url_ext_no_params+"?user_id="+post.user_id+"&model_id="+post.model_id;
				}
				else {
				ranker_url=ranker_url_no_params+"?user_id="+post.user_id;
                ranker_url_ext=ranker_url_ext_no_params+"?user_id="+post.user_id;
				}
			}
			else {ranker_url=ranker_url_default; ranker_url_ext = ranker_url_ext_default;}
			if (typeof post.minDetectTime !== 'undefined' && post.minDetectTime !== null && post.minDetectTime !== "" && post.minDetectTime !== "null") {
				$minDetectTime=post.minDetectTime;
			}
			if (typeof post.maxDetectTime !== 'undefined' && post.maxDetectTime !== null && post.maxDetectTime !== "" && post.maxDetectTime !== "null"){
				$maxDetectTime=post.maxDetectTime;
			}
			if (typeof post.category !== 'undefined' && post.category !== null && post.category !== "" && post.category !== "null"){
				$category=post.category;
			}
			if (typeof post.source_ip !== 'undefined' && post.source_ip !== null && post.source_ip !== "" && post.source_ip !== "null"){
				$source_ip=post.source_ip;
			}
			if (typeof post.target_ip !== 'undefined' && post.target_ip !== null && post.target_ip !== "" && post.target_ip !== "null"){
				$target_ip=post.target_ip;
			}
			if (typeof post.source_port !== 'undefined' && post.source_port !== null && post.source_port !== "" && post.source_port !== "null"){
				$source_port=post.source_port;
			}
			if (typeof post.target_port !== 'undefined' && post.target_port !== null && post.target_port !== "" && post.target_port !== "null"){
				$target_port=post.target_port;
			}
			if (typeof post.ruleset !== 'undefined' && post.ruleset !== null && post.ruleset !== "" && post.ruleset !== "null"){
				try {
				templateID=post.ruleset;
			if (templateID <= 0) {templateID = 3;}
				}
				catch(e) {
				 console.log("Parse error in ruleset:\n\""+post.ruleset+"\n Using default value of MaxIndex");
				 templateID=3;
				}
			}
			if (typeof post.MinIndex !== 'undefined' && post.MinIndex !== null && post.MinIndex !== "" && post.MinIndex !== "null"){
				try {
				MinIndex=post.MinIndex;
				if (MinIndex <= 0) {MinIndex = 1;}
				}
				catch(e) {
				 console.log("Parse error in MaxIndex:\n\""+post.MinIndex+"\n Using default value of MaxIndex");
				 MinIndex=1;
				}
			}
			if (typeof post.MaxIndex !== 'undefined' && post.MaxIndex !== null && post.MaxIndex !== "" && post.MaxIndex !== "null"){
				try {
				MaxIndex=post.MaxIndex;
				console.log("MaxIndex: "+MaxIndex+"\n");
				if (MaxIndex < 0) {MaxIndex = 1;}
				if (MaxIndex < MinIndex) {MaxIndex=MinIndex;}
				}
				catch(e) {
				 console.log("Parse error in MaxIndex:\n\""+post.MaxIndex+"\n Using default value of MaxIndex");
				 MaxIndex=50;
				}
			}
			try {
//				console.log("Query:"+post.query);
				query = JSON.parse(post.query); //{DetectTime: /2018-06-15.*/};
				//console.log("Parsed query="+JSON.stringify(query));

			} catch(e) {
				//console.log(e);
				if (post.query !== "") {//console.log("Parse error in Query:\n\""+post.query+"\"\nto MongoDB using fetch all query: {}");
				}
				query={}; // error in the above string - build an empty query, all documents will be retrieved
			}
			if (outHTML){

				client.query('SELECT json2html as template from ranking_json2html where id = $1',[json2htmlTemplateID], (err, resPGJ) => {
					//console.log(resPGJ.rows[0].template);
					var fread="";
					if ((!err)&&(resPGJ.rows[0]!= undefined)) {fread=resPGJ.rows[0].template; }
					else {fread=""; res.writeHead(500, {'Content-Type': 'text/html; charset=utf-8'}); res.end("Internal error: cannot access template from criteria-mapper database"); return console.log("Database of criteria-mapper is improperly initialized. Drop ranking_json2html table from criteria-mapper database and redeploy the module.\n")}
				    if (reorder){
						if (sortByPriority) { fread = fread.replace("{{formParam}}","\""+encodeUrl("query="+JSON.stringify(query)+"&MinIndex="+MinIndex+"&MaxIndex="+MaxIndex+"&ReorderOut=on&SortByPriority=on"+"&qID="+$qID+"&minDetectTime="+$minDetectTime+"&maxDetectTime="+$maxDetectTime+"&category="+$category+"&source_ip="+$source_ip+"&target_ip="+$target_ip+"&source_port="+$source_port+"&target_port="+$target_port+"&limitSQL="+$limitSQL)+"\"");}
						else {fread = fread.replace("{{formParam}}","\""+encodeUrl("query="+JSON.stringify(query)+"&MinIndex="+MinIndex+"&MaxIndex="+MaxIndex+"&ReorderOut=on"+"&qID="+$qID+"&minDetectTime="+$minDetectTime+"&maxDetectTime="+$maxDetectTime+"&category="+$category+"&source_ip="+$source_ip+"&target_ip="+$target_ip+"&source_port="+$source_port+"&target_port="+$target_port+"&limitSQL="+$limitSQL)+"\"");}
					}
					else {
						if (sortByPriority) {fread = fread.replace("{{formParam}}","\""+encodeUrl("query="+JSON.stringify(query)+"&MinIndex="+MinIndex+"&MaxIndex="+MaxIndex+"&SortByPriority=on"+"&qID="+$qID+"&minDetectTime="+$minDetectTime+"&maxDetectTime="+$maxDetectTime+"&category="+$category+"&source_ip="+$source_ip+"&target_ip="+$target_ip+"&source_port="+$source_port+"&target_port="+$target_port+"&limitSQL="+$limitSQL)+"\"");} 
						else {fread = fread.replace("{{formParam}}","\""+encodeUrl("query="+JSON.stringify(query)+"&MinIndex="+MinIndex+"&MaxIndex="+MaxIndex+"&qID="+$qID+"&minDetectTime="+$minDetectTime+"&maxDetectTime="+$maxDetectTime+"&category="+$category+"&source_ip="+$source_ip+"&target_ip="+$target_ip+"&source_port="+$source_port+"&target_port="+$target_port+"&limitSQL="+$limitSQL)+"\"");}
					}
					res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
					res.end(fread);
			});}
			else{
			//console.log("MinIndex: "+MinIndex+"MaxIndex: "+MaxIndex+"\n");
//POST request processed

			{
				//if (err) throw err;
				var template={};
				var MaxHandlingTime = 86400*1000; //Time in miliseconds
				//if (err) {return console.log("Error while connecting to MongoDB");}
				//var dbo = db.db(dbname);

//Getting set of MetaAlerts from DB
					meta_alerts.query('select array_to_json(array_agg(ma)) from \
					(select id, detecttime, category, maquality, rule, topassetcriticality, aggrid, source_ip, source_port, source_type, source_maxassetcriticality, source_hostname, target_ip, target_port, target_type, target_maxassetcriticality, target_hostname, protocol, maxcvss from meta_alerts where \
					($1::text IS NULL OR id = $1::text)\
					AND ($2::timestamp IS NULL OR detecttime >= $2)\
					AND ($3::timestamp IS NULL OR detecttime <= $3)\
					AND ($4::text IS NULL OR $4::text=ANY(category))\
					AND ($5::integer IS NULL OR $5::integer=ANY(source_port))\
					AND ($6::integer IS NULL OR $6::integer=ANY(target_port))\
					AND ($7::ipaddress IS NULL OR $7<<=ANY(source_ip))\
					AND ($8::ipaddress IS NULL OR $8<<=ANY(target_ip))\
					limit $9) ma;',[$qID,$minDetectTime,$maxDetectTime,$category,$source_port,$target_port,$source_ip,$target_ip,$limitSQL], (err, resultPG) => {	

//Processing set of MetaAlerts
					if (err) {res.writeHead(500, {'Content-Type': 'text/html; charset=utf-8'}); res.end("Internal error: cannot access meta-alerts database"); console.log("Error while executing query to PostgreSQL (POST):"+err); return null;}
                   //console.log("Tu MA from PGSQL"+JSON.stringify(resultPG));
				   console.log($qID+","+$minDetectTime+","+$maxDetectTime+","+$category+","+$source_ip+","+$target_ip+","+$source_port+","+$target_port);
					result=resultPG["rows"][0]["array_to_json"];
					if (!result) { result=[];}
					//client.connect();
					client.query('SELECT template as template, params as params from criteria_map where id = $1',[templateID], (err1, resPG) => {
											if (err1) {return console.log("Error while executing query to criteria_map:"+err1)}
					//console.log(resPG.rows[0].template);
					var templateFromPGSQL="";
					var paramsFromPGSQL="";
					if ((!err1)&&(resPG.rows[0] != undefined)) {templateFromPGSQL=resPG.rows[0].template; paramsFromPGSQL=resPG.rows[0].params;}
					else {res.writeHead(500, {'Content-Type': 'text/html; charset=utf-8'}); res.end("Internal error: cannot access mapping template in criteria-mapper database"); return console.log("Cannot retrieve mapping table from database. Check if database is properly initialized. \nYou may drop criteria_map table in criteria-mapper database and redeploy the criteria-mapper module.\n"); templateFromPGSQL={};}
					//client.end();
					//eval(fs.readFileSync('./mapper_template.js')+''); //Read the template for transform operation (Consider to change for safer alternative than eval)
					try {
//Read from file a template transforming values of MetaAlerts properties into values of Criteria, and its additional parameters (variables file)
					var fread =	templateFromPGSQL; //fs.readFileSync(STJStemplatefile,'utf8');
					var freadParam = paramsFromPGSQL;	;//fs.readFileSync(paramJSONfile,'utf8');
					var paramJSON=JSON.parse(freadParam);
//Loop through parameters and substitute parameters in the template by thier values
					for (var param in paramJSON) {
					 //console.log(param+": "+paramJSON[param]);	
					 fread = fread.replace(param, paramJSON[param]);
					}

//Remove newlines and '\' added for clarity in a template (without those operations the JSON will not parse)
					fread = fread.replace(/\\\r\n/g,'');
					fread = fread.replace(/\\\n/g,''); //Just in case to make a code fully cross platform
					fread = fread.replace(/\t*/g,'');
					//console.log(fread);
//Parse the template and convert it to the object
					template=JSON.parse(fread); 
					} catch(e) {
						console.log("Error while parsing template for Transform operation"+e);
						template={};
					}
//Do actual calculation of criteria using STJS library
				var parsed = ST.select({items: result})
                .transformWith(template)
                .root();
				//console.log("STJS parsed:"+JSON.stringify(parsed));
//Check which endpoint has been called and do relevant actions	
				if (req.url === '/criteria'){
					res.writeHead(200, {'Content-Type': 'application/json;charset=utf-8', 'Access-Control-Allow-Origin': '*'});
					criteria += JSON.stringify(parsed);
//If /criteria then send only criteria vectors
					res.end(criteria);
//					console.log("Sending criteria vectors");

				}
				else {	
//Otherwise prepare data to post JSON data to ranker (ranking-generator module): /rank-ext or /rank
				if (req.url === '/rank-ext' || req.url === '/rank-meta-alert' || req.url === '/get-learning-set' ){ //Endpoint returning ranking with full vectors

//					console.log("Sending criteria vectors in form of JSON objects to rank-ext");
				    ranker_url_tmp=ranker_url_ext;
				}
//Send request to one of ranker endpoints: /rank-ext (returns full vector of criteria with priority) or /rank (returns only Priority and IDs)
			    console.log("Ranker url:"+ranker_url_tmp+"\n");
				request({
					url: ranker_url_tmp,
					method: "POST",
					json: true,   // <--Very important!!!
					body: parsed  //criteria //parsed - ranking-generator requires JSON array
				}, function (error, response, body){
//Process response from ranker (ranking-generator module)
					 if (!error && response.statusCode == 200 &&  typeof body !== 'undefined'){
						res.writeHead(200, {'Content-Type': 'application/json','Access-Control-Allow-Origin': '*'});
//if /rank-meta-alert endpoint of criteria-mapper has been called then fetch Meta-Alerts from DB and merge them with criteria in one object
						if (req.url === '/rank-meta-alert' || req.url === '/get-learning-set' ){
						var count=0;
						//Sort body by Priority
						if (sortByPriority){
						function GetSortOrder(prop) {  
							return function(a, b) {  
							if (a[prop] > b[prop]) {  
								return 1;  
							} else if (a[prop] < b[prop]) {  
							return -1;  
							}  
							return 0;  
							}  
						}  
						body.sort(GetSortOrder("Priority"));
						}
						
						var LookupMetaAlert = {};
						result.forEach(function(key) {
										LookupMetaAlert[key.id] = key;

						});
									
	//Loop through ranking-generator response - (IMPORTANT: be aware of asynchronous processing of database operations):
						 body.forEach(function(item,index){
							count=count+1;
							if (count > MaxIndex){ return;}
							if (count >= MinIndex) {
						//	if (index >= MaxIndex){ return;}
						//	if (index >= MinIndex-1) {
						//	  query = JSON.parse("{\"ID\":\""+item['ID']+"\"}"); //Example: query='{"ID":"ae516697-9acf-44b3-ad70-be9753e93bc2"}';

	//Fetch Metal-Alert from DB	  

							  //console.log("Item",JSON.stringify(item));
						//meta_alerts.query('select array_to_json(array_agg(ma)) from (select * from meta_alerts where id=$1) ma;',[item['ID']], (err, unorderedMetaAlertPG) => {
							//  if (err) {return console.log("Error while executing query to MongoDB:"+err)}
							 // console.log("unorderedMetaAlertPG="+JSON.stringify(unorderedMetaAlertPG)+"item['ID']"+item['ID']);
							 // unorderedMetaAlert=unorderedMetaAlertPG["rows"][0]["array_to_json"][0];
						var unorderedMetaAlert=LookupMetaAlert[item['ID']];
						//console.log("LookupMetaAlert"+JSON.stringify(unorderedMetaAlert)+"\n");
	//Combine Meta-Alert with Criteria including priority
								    //Criteria=item; //body[item];	
									var Criteria = {};
									if (reorder){
									Object.keys(item).sort().forEach(function(key) {
										Criteria[key] = item[key];
									});
									}
									else {
										Criteria=item;
									}
									
									var MetaAlert = {};
									if (reorder){
									Object.keys(unorderedMetaAlert).sort().forEach(function(key) {
										MetaAlert[key] = unorderedMetaAlert[key];
									});
									}
									else {
										MetaAlert = unorderedMetaAlert;
									}
									MetaAlertWithCriteria={Criteria, MetaAlert};
								//	console.log("MinIndex,index"+MinIndex+","+index);
									if (index === MinIndex-1){res.write("[");}
									if (index === MaxIndex-1 || index === body.length-1) {res.end(JSON.stringify(MetaAlertWithCriteria)+"]");}
									else {res.write(JSON.stringify(MetaAlertWithCriteria)+",");}
					//		  });
							}
						 })
						}
//END if /rank-meta-alert
//Else return directly output of the ranking-generator
						else { res.end(JSON.stringify(body));}

					 //console.log("Response from ranker:\n"+JSON.stringify(body));
					 }
					 else {console.log("Error in POST to ranker:"+error+"\nSending criteria vectors as fallback response\n");
						res.writeHead(200, {'Content-Type': 'application/json;charset=utf-8', 'Access-Control-Allow-Origin': '*'});
						criteria += JSON.stringify(parsed);
						res.end(criteria);

					 }
					 
				});
				}
				

				//console.log("Post response:"+criteria);
				console.log("Response to POST has been sent");
//Request has been processed and response send	

			});});
		};//);
			}
		});
		}
	else 	
	if (req.method === 'OPTIONS') {
		
	 res.writeHead(200, {'Content-Type': 'application/json','Access-Control-Request-Headers': 'Access-Control-Allow-Origin, Authorization','Access-Control-Allow-Headers': 'Access-Control-Allow-Origin, Authorization','Access-Control-Allow-Origin': '*'});
	 res.end();	
	 }
    else {	
	if (req.method === 'GET') {
		if ((req.url === '/form') ||(req.url === '/form-gui')) {
			   
//In case of GET /form request produce simple landing page to show possible POST requests
        //console.log("GET");
		var style="";
		var formCssID=1;
		client.query('SELECT css as css from form_css where id = $1',[formCssID], (err, resPGcss) => {
				//	console.log(resPGcss.rows[0].css);	
			try {
			style =	resPGcss.rows[0].css;//fs.readFileSync('./form.css','utf8');
			} catch (e) {
				console.log('Error'+e);
				style="";
			}
        var html ='<html><head><meta charset="UTF-8"></head>';		
		    html+='<style>'+style+'</style>';
		    html+='<body><h1>Meta-Alert Prioritisaiton Module</h1><p>Provide optional filtering criteria to select meta-alerts for prioritisation.</p>';
			html+='<form method="post" action="/rank-meta-alert">';
			html+='<div><label for="qID">ID: </label><input type="text" name="qID" /></div>';
			html+='<div><label for="minDetectTime">Min. detect time: </label><input type="text" name="minDetectTime" /></div>';
			html+='<div><label for="maxDetectTime">Max. detect time: </label><input type="text" name="maxDetectTime" /></div>';
			html+='<div><label for="category">Category: </label><input type="text" name="category" /></div>';
			html+='<div><label for="source_ip">Source IP (e.g. 192.168.0.1): </label><input type="text" name="source_ip" /></div>';
			html+='<div><label for="source_port">Source port</label><input type="text" name="source_port" /></div>';
			html+='<div><label for="target_ip">Target IP (e.g. 192.168.0.2): </label><input type="text" name="target_ip" /></div>';
			html+='<div><label for="target_port">Target port: </label><input type="text" name="target_port" /></div>';
			html+='<div><label for="limitSQL">Limit for SQL: </label><input type="text" name="limitSQL" /></div>';
			html+='<div><label for="MinIndex">MinIndex: </label><input type="number" name="MinIndex" /></div>';		
			html+='<div><label for="MaxIndex">MaxIndex:</label><input type="number" name="MaxIndex" /></div>';
			html+='<div><label for="HtmlOut">Results in HTML (valid only for ranking with criteria and full meta-alerts):</label><input type="checkbox" name="HtmlOut" /></div>';
			html+='<div><label for="ReorderOut">Reorder properties of the returned objects:</label><input type="checkbox" name="ReorderOut" /></div>';
			html+='<div><label for="SortByPriority">Sort by priority:</label><input type="checkbox" name="SortByPriority" /></div>';
			html+='<div><label for="submit">Get ranking with criteria and full meta-alerts: </label></div>';
			html+='<div class="button"><button type="submit">Rank meta-alerts</button></div>';
			html+='<div><label for="query">Get ranking (IDs only): </label></div>';
			html+='<div class="button"><button type="submit" formaction="/">Rank meta-alerts</button></div>';
			html+='<div><label for="query">Get criteria: </label></div>';
			html+='<div class="button"><button type="submit" formaction="/criteria">Get criteria values for meta-alerts</button></div>';
			html+='<div><label for="query">Get ranking with criteria: </label></div>'
			html+='<div class="button"><button type="submit" formaction="/rank-ext">Rank meta-alerts</button></div>';
			html+='<div><label for="query">Get examples for learning set: </label></div>';
			html+='<div class="button"><button type="submit" formaction="/get-learning-set">Get learning set</button></div>';
			html+='</form></body>';
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(html);
		});}
		else {
 //       console.log("GET");
        var body = '';
		var criteria = '';
		var ranker_url_tmp=ranker_url;
//Receiving data from GET: 

 {
//Processing data from GET request

			var parsedUrl = url_module.parse(req.url);  
			var post = querystring.parse(parsedUrl.query);

//			console.log('Get query:'+JSON.stringify(post)+'\n');
			var query= {};
			var MinIndex=1; //Default value for the index of the first object in the ranking that should be shown - usefull for pagination of results
			var MaxIndex=50; //Default value for the index of the last object in the ranking that should be shown - usefull for pagination of results
			var templateID=3;
			var $limitSQL=100000;
			var $qID=null;
			var	$minDetectTime=null;//'2018-09-13T21:54:01Z';
			var	$maxDetectTime=null;//'2018-09-15T21:54:01Z';
			var $category=null;
			var	$source_ip=null; //'[192.168.0.1,192.168.0.2]';
			var	$target_ip=null;
			var	$source_port=null;
			var	$target_port=null;
			var sortByPriority=0;
			if (typeof post !== 'undefined' && typeof post['limitSQL'] !== 'undefined' && post['limitSQL'] !== null && post['limitSQL'] !== "" && post['limitSQL'] !== "null"){
				$limitSQL=post['limitSQL'];
			}
			if (typeof post !== 'undefined' && typeof post['qID'] !== 'undefined' && post['qID'] !== null && post['qID'] !== "" && post['qID'] !== "null"){
				$qID=post['qID'];
			}
			
			if (typeof post !== 'undefined' && typeof post['user_id'] !== 'undefined' && post['user_id'] !== null && post['user_id'] !== "" && post['user_id'] !== "null"){
				if (typeof post['model_id'] !== 'undefined' && post['model_id'] !== null && post['model_id'] !== "" && post['model_id'] !== "null"){
				ranker_url=ranker_url_no_params+"?user_id="+post['user_id']+"&model_id="+post['model_id'];
                ranker_url_ext=ranker_url_ext_no_params+"?user_id="+post['user_id']+"&model_id="+post['model_id'];
				}
				else {
				ranker_url=ranker_url_no_params+"?user_id="+post['user_id'];
                ranker_url_ext=ranker_url_ext_no_params+"?user_id="+post['user_id'];
				}
			}
			else {ranker_url=ranker_url_default; ranker_url_ext = ranker_url_ext_default;}

			if (typeof post !== 'undefined' && typeof post['minDetectTime'] !== 'undefined' && post['minDetectTime'] !== null && post['minDetectTime'] !== "" && post['minDetectTime'] !== "null"){
				$minDetectTime=post['minDetectTime'];
				console.log("minDetectTime"+$minDetectTime);
			}
			if (typeof post !== 'undefined' && typeof post['maxDetectTime'] !== 'undefined' && post['maxDetectTime'] !== null && post['maxDetectTime'] !== "" && post['maxDetectTime'] !== "null"){
				$maxDetectTime=post['maxDetectTime'];
			}
			if (typeof post !== 'undefined' && typeof post['category'] !== 'undefined' && post['category'] !== null && post['category'] !== "" && post['category'] !== "null"){
				$category=post['category'];
			}
			if (typeof post !== 'undefined' && typeof post['source_ip'] !== 'undefined' && post['source_ip'] !== null && post['source_ip'] !== "" && post['source_ip'] !== "null"){
				$source_ip=post['source_ip'];
			}
			if (typeof post !== 'undefined' && typeof post['target_ip'] !== 'undefined' && post['target_ip'] !== null && post['target_ip'] !== "" && post['target_ip'] !== "null"){
				$target_ip=post['target_ip'];
			}
			if (typeof post !== 'undefined' && typeof post['source_port'] !== 'undefined' && post['source_port'] !== null && post['source_port'] !== "" && post['source_port'] !== "null"){
				$source_port=post['source_port'];
			}
			if (typeof post !== 'undefined' && typeof post['target_port'] !== 'undefined' && post['target_port'] !== null && post['target_port'] !== "" && post['target_port'] !== "null"){
				$target_port=post['target_port'];
			}
			if (typeof post !== 'undefined' && typeof post['SortByPriority'] !== 'undefined' && post['SortByPriority'] !== null && post['SortByPriority'] !== "" && post['SortByPriority'] !== "null"){
			    if (post['SortByPriority']==="on" || (post['SortByPriority']==="true")){sortByPriority=1; console.log("Sorting by Priority is on");}
			}
			
			

			if (typeof post !== 'undefined' && typeof post['rulset'] !== 'undefined' && post['ruleset'] !== null && post['ruleset'] !== ""){
				try {
				templateID=post['ruleset'];
				if (templateID <= 0) {templateID = 3;}
				}
				catch(e) {
				 console.log("Parse error in ruleset:\n\""+post['ruleset']+"\n Using default value of MaxIndex");
				 templateID = 3;
				}
			}

			if (typeof post !== 'undefined' && typeof post['MinIndex'] !== 'undefined' && post['MinIndex'] !== null){
				try {
				MinIndex=post['MinIndex'];
				if (MinIndex <= 0) {MinIndex = 1;}
				}
				catch(e) {
				 console.log("Parse error in MinIndex:\n Using default value of MinIndex");
				 MinIndex=1;
				}
			}
			if (typeof post !== 'undefined' && typeof post['MaxIndex'] !== 'undefined' && post['MaxIndex'] !== null){
				try {
				MaxIndex=post['MaxIndex'];
				if (MaxIndex < 0) {MaxIndex = 1;}
				if (MaxIndex < MinIndex) {MaxIndex=MinIndex;}
				}
				catch(e) {
				 console.log("Parse error in MaxIndex:\n Using default value of MaxIndex");
				 MaxIndex=50;
				}
			}
			try {
				query = JSON.parse(post['query']);
			} catch(e) {
				if (post['query']!== "" && typeof post['query'] !== 'undefined') {console.log("Parse error in Query (from GET):"+ post['query'] + "to MongoDB using fetch all query: {}");}
				query={}; // error in the above string - build an empty query, all documents will be retrieved
			}
//GET request processed

			{
				//if (err) throw err;
				var template={};
				var MaxHandlingTime = 86400*1000; //Time in miliseconds
				//if (err) {return console.log("Error while connecting to MongoDB");}
				//var dbo = db.db(dbname);
//Getting set of MetaAlerts from DB
				//dbo.collection(dbcollectionname).find(query).toArray(function(err, result) {
				//	meta_alerts.query('select array_to_json(array_agg(meta_alerts)) from meta_alerts;',[], (err, resultPG) => {
					meta_alerts.query('select array_to_json(array_agg(ma)) from \
					(select id, detecttime, category, maquality, rule, topassetcriticality, aggrid, source_ip, source_port, source_type, source_maxassetcriticality, source_hostname, target_ip, target_port, target_type, target_maxassetcriticality, target_hostname, protocol, maxcvss from meta_alerts where \
					($1::text IS NULL OR id = $1::text)\
					AND ($2::timestamp IS NULL OR detecttime >= $2)\
					AND ($3::timestamp IS NULL OR detecttime <= $3)\
					AND ($4::text IS NULL OR $4::text=ANY(category))\
					AND ($5::integer IS NULL OR $5::integer=ANY(source_port))\
					AND ($6::integer IS NULL OR $6::integer=ANY(target_port))\
					AND ($7::ipaddress IS NULL OR $7<<=ANY(source_ip))\
					AND ($8::ipaddress IS NULL OR $8<<=ANY(target_ip))\
					limit $9) ma;',[$qID,$minDetectTime,$maxDetectTime,$category,$source_port,$target_port,$source_ip,$target_ip,$limitSQL], (err, resultPG) => {
                  
//Processing set of MetaAlerts
					if (err) {res.writeHead(500, {'Content-Type': 'text/html; charset=utf-8'}); res.end("Internal error: cannot access meta-alerts database"); return console.log("Error while executing query to PostgreSQL:"+err)}
					//console.log("MA form PGSQL"+JSON.stringify(resultPG["rows"][0]["array_to_json"]));
					result=resultPG["rows"][0]["array_to_json"];	
					if (!result) { result=[];}
					//client.connect();
					console.log("TemptemplateID:"+templateID);
					client.query('SELECT template as template, params as params from criteria_map where id = $1',[templateID], (err1, resPG1) => {
					if (err1) {return console.log("Problem with getting criteria_map"+err1);}	
					//console.log(resPG1.rows[0].template);
					var templateFromPGSQL="";
					var paramsFromPGSQL="";
					if ((!err)&&(resPG1.rows[0]!= undefined))  {templateFromPGSQL=resPG1.rows[0].template; paramsFromPGSQL=resPG1.rows[0].params;}
					else {res.writeHead(500, {'Content-Type': 'text/html; charset=utf-8'});res.end("Internal error: cannot access mapping template in criteria-mapper database"); return console.log("Database of criteria-mapper is improperly initialized. Drop criteria_map table from criteria-mapper database and redeploy the module.\n")}

					try {
//Read from file a template transforming values of MetaAlerts properties into values of Criteria, and its additional parameters (variables file)
					var fread =	templateFromPGSQL; //fs.readFileSync(STJStemplatefile,'utf8');
					var freadParam = paramsFromPGSQL;	//fs.readFileSync(paramJSONfile,'utf8');
					var paramJSON=JSON.parse(freadParam);
//Loop through parameters and substitute parameters in the template by thier values
					for (var param in paramJSON) {
					 //console.log(param+": "+paramJSON[param]);	
					 fread = fread.replace(param, paramJSON[param]);
					}

//Remove newlines and '\' added for clarity in a template (without those operations the JSON will not parse)
					fread = fread.replace(/\\\r\n/g,'');
					fread = fread.replace(/\\\n/g,''); //Just in case to make a code fully cross platform
					fread = fread.replace(/\t*/g,'');
					//console.log(fread);
//Parse the template and convert it to the object
					template=JSON.parse(fread); 
					} catch(e) {
						console.log("Error while parsing template for Transform operation"+e);
						template={};
					}
//Do actual calculation of criteria using STJS library
				var parsed = ST.select({items: result})
                .transformWith(template)
                .root();
				//console.log("STJS parsed:"+JSON.stringify(parsed));
//Check which endpoint has been called and do relevant actions	
				if (req.url.match(/^\/criteria.*/)){
					res.writeHead(200, {'Content-Type': 'application/json;charset=utf-8', 'Access-Control-Allow-Origin': '*'});
					criteria += JSON.stringify(parsed);
//If /criteria then send only criteria vectors
					res.end(criteria);
					console.log("Sending criteria vectors");
					//db.close();
				}
				else {	
//Otherwise prepare data to post JSON data to ranker (ranking-generator module): /rank-ext or /rank
				if (req.url.match(/^\/rank-ext.*/) || req.url.match(/\/rank-meta-alert.*/)){ //Endpoint returning ranking with full vectors
//					console.log("Sending criteria vectors in form of JSON objects to rank-ext");
				    ranker_url_tmp=ranker_url_ext;
				}
//Send request to one of ranker endpoints: /rank-ext (returns full vector of criteria with priority) or /rank (returns only Priority and IDs)
				request({
					url: ranker_url_tmp,
					method: "POST",
					path: "?user_id=protective",
					json: true,   // <--Very important!!!
					body: parsed //criteria //parsed - ranking-generator requires JSON array
				}, function (error, response, body){
//Process response from ranker (ranking-generator module)
					 if (!error && response.statusCode == 200 && typeof body !== 'undefined'){
						res.writeHead(200, {'Content-Type': 'application/json;charset=utf-8', 'Access-Control-Allow-Origin': '*'});
//if /rank-meta-alert endpoint of criteria-mapper has been called then fetch Meta-Alerts from DB and merge them with criteria in one object
						if (req.url.match(/\/rank-meta-alert.*/)){
						var count=0;
						//Sort body by Priority
						if (sortByPriority){
						function GetSortOrder(prop) {  
							return function(a, b) {  
							if (a[prop] > b[prop]) {  
								return 1;  
							} else if (a[prop] < b[prop]) {  
							return -1;  
							}  
							return 0;  
							}  
						}  
						body.sort(GetSortOrder("Priority"));
						}

						var LookupMetaAlert = {};
						result.forEach(function(key) {
										LookupMetaAlert[key.id] = key;
						});
	//Loop through ranking-generator response - (IMPORTANT: be aware of asynchronous processing of database operations):
						 body.forEach(function(item,index){
							count=count+1;
							if (count > MaxIndex){ return;}
							if (count >= MinIndex) {
								
						//	  query = JSON.parse("{\"ID\":\""+item['ID']+"\"}"); //Example: query='{"ID":"ae516697-9acf-44b3-ad70-be9753e93bc2"}';
							  //dbx = db.db(dbname);
	//Fetch Metal-Alert from previous query result 	  
 						//	  meta_alerts.query('select array_to_json(array_agg(ma)) from (select * from meta_alerts where id=$1) ma;',[item['ID']], (err, MetaAlertPG) => {
								//if (err) {return console.log("Error while fetching meta-alerts by ID :"+err)}
								//MetaAlert=MetaAlertPG["rows"][0]["array_to_json"][0];
								var MetaAlert=LookupMetaAlert[item['ID']];
								//console.log("MetaAlertPG"+JSON.stringify(MetaAlert));
	//Combine Meta-Alert with Criteria including priority
								Criteria=item; //body[item];
								MetaAlertWithCriteria={Criteria, MetaAlert};
	//							console.log("MinIndex,index"+MinIndex+","+index);
								if (index === MinIndex-1){res.write("[");}
								if (index === MaxIndex-1 || index === body.length-1) {res.end(JSON.stringify(MetaAlertWithCriteria)+"]");}
								else {res.write(JSON.stringify(MetaAlertWithCriteria)+",");}
						//	  });
							}
						 })
						}
//END if /rank-meta-alert
//Else return directly output of the ranking-generator
						else { res.end(JSON.stringify(body));}

					 //console.log("Response from ranker:\n"+JSON.stringify(body));
					 }
					 else {console.log("Error in GET to ranker:"+error+"\nSending criteria vectors as fallback response\n");
						res.writeHead(200, {'Content-Type': 'application/json;charset=utf-8', 'Access-Control-Allow-Origin': '*'});
						criteria += JSON.stringify(parsed);
						res.end(criteria);
						//db.close();
					 }
				});
				}

				console.log("Response to GET has been sent");
//Request has been processed and response send	
		});	});
		};//);
		 
		}
 
		}
		}
}}
);
server.listen(port, host);
console.log('Listening at http://' + host + ':' + port);
