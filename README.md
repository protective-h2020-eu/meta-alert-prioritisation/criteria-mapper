# Criteria mapper
Criteria Mapper (criteria-mapper) is part of Meta-Alert Prioritisation (MAP) module.  The criteria-mapper serves as a wrapper between GUI, ranking-generator and meta-alerts database (PostgreSQL). It converts values of meta-alerts properties into values of prioritisation criteria based on defined and customizable templates. 
Criteria-mapper as input takes query parameters that are past to SQL query and used to select data for prioritisation. The fetched data are transformed into vectors of criteria values, processed by Ranking Generator (ranking-generator) module and send back to user in form of JSON or HTML. 

The web-forms provided by criteria-mapper allows to specify the following filtering criteria: meta-alert id, time range (min. and max. value of 'detecttime' field), category of meta-alert, source and target IPs, source and target ports, limit for number of SQL query results (it limits the number of meta-alerts that are passed to ranking-generator module), define the slice of data to be returned (e.g. MinIndex and MaxIndex of items returned by ranking-generator), select type of output JSON or HTML (default is JSON)  and if needed reorder the JSON properties by sorting them alphabetically by keys.
The produced output consist of priority (decision class) as provided by ranking-generator (based on MCDA model), values of criteria and values of properties of meta-alerts. The properties shown in HTML output can be selected through adjustment of the template file transforming JSON to HTML. 

 Examples of criteria that can be used by prioritisation includes (but are not limited to) the following ones: an asset criticality provided by Context Awareness Module (attack Source/TargetAssetCriticality), a severity of attack category – categories of attacks are mapped into classes of severity from low to critical, meta-alert quality as computed on the basis of input from Trust Module during correlation process, time to due date – time that remains to the moment when the meta-alert should be processed by the operator (due date is the time of detection (DetectTime) plus maximal handling time specified in criteria-mapper template for criteria computations), time that passed from detect time (some form of alert freshness). 

The criteria-mapper allows for showing priorities and values of criteria side by side with meta-alerts properties (e.g. Source, Target,  ID-meta-alert ID, AggrID – ID of correlated alerts) or just priorities and meta-alerts IDs, or priorities with criteria and IDs, or criteria vectors without assigned priorities.


## Prerequisites:

### Node.js 
More info: https://nodejs.org/en/

### Postgresql Database 
More info: https://www.postgresql.org/

The name of default database with configuration data is *criteria-mapper*. The database parameters are customizable in criteria-mapper.js file.
The dump of initial database (**criteria-mapper.sql**) is provided along with the source code of criteria-mapper (https://gitlab.com/protective-h2020-eu/meta-alert-prioritisation/criteria-mapper/blob/master-postgresql/criteria-mapper.sql). 
The module uses also *mentat_events* database as a source of meta-alerts. 
### Npm modules for Node.js 

#### Postgrsql driver 
npm install pg

More info: https://www.npmjs.com/package/pg

#### STJS: 
npm install STJS 

More info: https://selecttransform.github.io/site/

# How to install?
## Clone repository
git clone -b master-postgresql git@gitlab.com:protective-h2020-eu/meta-alert-prioritisation/criteria-mapper.git

## Update configuration
Edit parameters in: criteria-mapper\criteria-mapper.js

Optionally (if needed): 
1. Customize mappings of meta-alerts properties into criteria. The mappings are defined in a *criteria_map* table of the *criteria-mapper* database. The table has three colums: *id* of the template, *template* (see below for example) and *params* - additional parameters for the template (see below for details in "Additional configuration and customization" section ). In current configuration the template with *id* equal to *3* is used (the remaining two templates in database are compatible with previous versions of meta-alerts and will be dicarded in future releases).

2. Adjust JSON to HTML conversion template (see: *criteria-mapper* database, *ranking_json2html* table, the table has only tow colums: *id* and *json2html*) - see below for details.

## Start Node.js
cd criteria-mapper

node criteria-mapper.js
## Start browser
Go to (in default configuration): http://127.0.0.1:3001/form or /form-gui 
or http://host:port/form (if you changed default parameters)

You should see the web-form shown below.

![alt text](screenshots/WebForm.png "Web-forms for testing MAP module APIs (/form or /form-gui) ")

Submit filtering parameters (e.g. Limit for SQL: 5, it corresponds to passing limitSQL=10 when using API), that will be used to filter meta-alerts from PostgreSQL DB (*mentat_events* database in default configuration).

# Additional configuration and customization
## Parameters in criteria-map table (criteria-mapper database)
Example parameters from *params* column of a *criteria-map* table of the *criteria-mapper* database for the default template with *id* equal to 3. 
The table has three colums: *id* of the template, *template* and *params*.

file: mapper_params_for_template.js

```javascript
{
"_MaxHandlingTime_": 86400000
}
```
## Template for transform operation (mapping of meta-alerts into criterial space)
Example template from *template* column of a *criteria_map* table of the *criteria-mapper* database for the default template with *id* equal to 3. 
The table has three colums: *id* of the template, *template* and *params*.

More info about general template syntax supported by STJS: 
https://selecttransform.github.io/site/transform.html

The template allows use of standard Javascript functions inline (see: *TimeToDueDate* or *SeverityForAttackCategory* definitions in the template below).

Additionaly, two features has been implemented to extend funcionality of the templates for Protective project:
* It is possible to use parameters defined in *params* column" (e.g. noticed \_MaxHandlingTime\_ in the template). 
* Within the template you can use "\" followed by newline and tabs for better readability of the template (see: SeverityForAttackCategory).

```json

	{
	"{{#each items}}": {
		"TargetAssetCriticality": "{{\
		(function(scale) {\
			if (scale == 4) {return 'critical';}; \
			if (scale == 3) {return 'high';}; \
			if (scale == 2) {return 'med';}; \
			if (scale == 1) {return 'low';}; \
		   return 'NA';})(target_maxassetcriticality)}}",
		"SourceAssetCriticality": "{{(function(scale) {\
			if (scale == 4) {return 'critical';}; \
			if (scale == 3) {return 'high';}; \
			if (scale == 2) {return 'med';}; \
			if (scale == 1) {return 'low';}; \
		   return 'NA';})(source_maxassetcriticality)}}",
	    "TimeToDueDate": "{{(((Date.parse(detecttime)+_MaxHandlingTime_)-Date.now())/60000)}}",
		"TimeFromDetectTime": "{{(((Date.now()-(60*1000*(new Date().getTimezoneOffset())))-Date.parse(detecttime))/60000)}}",
		"MAQuality": "{{(function(quality) {\
			if (quality == 99.0) {return 0.5}\
		return quality;})(maquality)}}",
		"AttackSourceReputationScore": "{{\
		(function(test){\
			if (test) { \
			  return test;}\
		return 0.5;})(typeof(source_reputationscore)!=='undefined'?source_reputationscore:0.5)}}",
		"SeverityForAttackCategory": "{{\
		  (function(category) {\
			if (RegExp('Attempt.Exploit*').test(category)) {return 'critical';}; \
			if (RegExp('Intrusion.*').test(category)) {return 'critical';}; \
			if (RegExp('Availability.DoS*').test(category)) {return 'critical';}; \
			if (RegExp('Vulnerable.Config*').test(category)) {return 'high';}; \
			if (RegExp('Anomaly.Traffic*').test(category)) {return 'high';}; \
			if (RegExp('Malware*').test(category)) {return 'high';}; \
			if (RegExp('Attempt.Login*').test(category)) {return 'med';}; \
			if (RegExp('Recon.Scanning*').test(category)) {return 'low';}; \
		   return 'med';})(category)}}",		
		"MaxCVE": "{{0.0}}",
		"ID": "{{\
		  (function(id) {\
			if (RegExp(/^[0-9a-f]{8}-?[0-9a-f]{4}-?[1-5][0-9a-f]{3}-?[89ab][0-9a-f]{3}-?[0-9a-f]{12}$/i).test(id)) {return id;}; \
			return '00000000-b4ac-11e7-9460-002564d9514f';})(id)}}"
	}  
}
```
## Template for JSON to HTML conversion
Prioritised Meta-alerts can be presented in HTML form, for that puspose json2html templates are provided in *criteria-mapper* database (see *rankning_json2html* table). 
Two templates are provided - one for collecting data for learning priorities (template with id=4) and one for visualisaiton of prioritised data.
Template (with id=3) - visualisation:

```javascript
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sortable Criteria and Meta-alert table</title>
<style>
table {
    border-spacing: 0;
    width: 140%;
    border: 1px solid #ddd;
    table-layout: fixed;
}

th {
    cursor: pointer;
}

th, td {
    text-align: left;
    padding: 16px;
	word-wrap:break-word;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

td[Priority="1"]{
 background-color: #de1c24
}
td[Priority="2"]{
 background-color: #dc8c00
}
td[Priority="3"]{
 background-color: #f0c800
}
td[Priority="4"]{
 background-color: #3697dd
}
td[Priority="5"]{
 background-color: #b3fedf
}

div.target {
    width: 100%;
    height: 100px;
    margin: 0;
    padding: 0;
    overflow: auto;
}
div.source {
    width: 100%;
    height: 100px;
    margin: 0;
    padding: 0;
    overflow: auto;
}
</style>
</head>
<body onload="getContent()">

<h2>Criteria and Meta-alerts combined</h2>

<p id="demo"></p>

<script>
function sortTable(n) { //This function needs optimization - change of sorting algorithm
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("MAcriteria");
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

function getContent(){
var obj, dbParam, xmlhttp, myObj, x, txt = "";

refObjText= '{' + 
	'"Criteria": {' + 
	'"Priority": "",' +
//	'"ID": "",' + 
	'"SourceAssetCriticality": "",' + 
	'"TargetAssetCriticality": "",' +
	'"SeverityForAttackCategory": "",' + 
	'"MAQuality": "",' + 	
	'"TimeToDueDate": "",' + 
	'"TimeFromDetectTime": "",' + 
//	'"AttackSourceReputationScore": "",' + 
	'"MaxCVE": ""' + 
	'},' + 
	'"MetaAlert": {' + 
//	'"id": "",' + 
	'"detecttime": "",' + 
	'"category": "",' + 
//	'"topassetcriticality": "",' + 
	'"rule": "",' + 
//	'"maquality": "",' + 
	'"source_ip": "",' +
    '"source_port": "",' +
    '"source_type": "",' +
    '"source_maxassetcriticality": "",'+
    '"source_hostname": "",' +
    '"target_ip": "",' +
    '"target_port": "",' +
    '"target_type": "",' +
//    '"target_MaxAssetCriticality": "",' +
    '"target_hostname": "",' +
    '"protocol": "",' +
//    '"event": "",' +
//    '"Source_reputationscore": "",' +	
	'"rule": "",' +	
    '"maxcvss": "",' +	
	'"id": "",' + 
	'"aggrid": ""' + 
 '}}';
 
headersObjText= '{' + 
	'"Criteria": "Criteria",' + 
	'"Priority": "Priority",' +
	'"ID": "Id",' + 
	'"SourceAssetCriticality": "Source asset criticality",' + 
	'"TargetAssetCriticality": "Target asset criticality",' +
	'"SeverityForAttackCategory": "Severity for attack category",' + 
	'"MAQuality": "Quality",' + 	
	'"TimeToDueDate": "Time to due date",' + 
	'"TimeFromDetectTime": "Time from detection",' + 
	'"AttackSourceReputationScore": "Attack source reputation",' + 
	'"MaxCVE": "Max CVSS",' + 
	'"MetaAlert": "Meta-alert",' + 
	'"id": "Id",' + 
	'"detecttime": "Time of detection",' + 
	'"category": "Category",' + 
	'"topassetcriticality": "Top asset criticality",' + 
	'"rule": "Meta-alert correlation rule",' + 
	'"maquality": "Quality",' + 
	'"source_ip": "Source IP",' +
    '"source_port": "Source Port",' +
    '"source_type": "Source Type",' +
    '"source_maxassetcriticality": "Maximal source asset criticality",'+
    '"source_hostname": "Source hostname",' +
    '"target_ip": "Target IP",' +
    '"target_port": "Target port",' +
    '"target_type": "Target type",' +
    '"target_MaxAssetCriticality": "Maximal target asset criticality",' +
    '"target_hostname": "Target hostname",' +
    '"protocol": "Protocol",' +
    '"event": "Event",' +
    '"Source_reputationscore": "Source reputation",' +	
	'"rule": "Meta-alert correlation rule",' +	
	'"maxcvss": "Max CVSS",' +	
	'"id": "Id",' + 
	'"aggrid": "IDs of correlated alerts"' + 
 '}';
 
refObj=JSON.parse(refObjText);
headersObj=JSON.parse(headersObjText);
formParam ="MinIndex=1&MaxIndex=3";
xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        myObj = JSON.parse(this.responseText);
		count=0;
		head="";
        for (x in myObj) {
		txt += "<tr>";
		count+=1;
		if (count ===1) {head+= "<tr>"; i=0}
		  for (y in refObj) {
		   for (z in refObj[y]){
		    if (count ===1){head+="<th onclick=\"sortTable("+i+")\">"+headersObj[z]+"</th>";i+=1;}
//			if (z === "Source"){
//					source="";
//					for (s in refObj[y][z]){
//						source+=s+":"+myObj[x][y][z][0][s]+"<br>";
//					}	
//					txt += "<td><div class=source>" + source + "</div></td>";
//				}
//				else {
//					if (z === "Target"){
//						target="";
						//tmpobj=JSON.parse(myObj[x][y][z]);
//						for (t in refObj[y][z]){
							//target+=t+":"+tmpobj[0][t]+"<br>";
//							target+=t+":"+myObj[x][y][z][0][t]+"<br>";
						//target+=myObj[x][y][z];
//					}	
//					txt += "<td><div class=target>" + target + "</div></td>";
//					}else{
					if (z === "Priority"){
					txt += "<td "+z+"="+JSON.stringify(myObj[x][y][z])+">" + JSON.stringify(myObj[x][y][z]) + "</td>";
					}
					else{
					txt += "<td>" + JSON.stringify(myObj[x][y][z]) + "</td>";
					}
//				}
//			}
			}
		  }
		txt += "</tr>";  
		if (count ===1) {head+= "</tr>";}
        }
        txt = "<table id='MAcriteria' >"+ head+txt;
		txt += "</table>"        
        document.getElementById("demo").innerHTML = txt;
    }
};
xmlhttp.open("POST", "/rank-meta-alert", true);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send({{formParam}});
}
</script>
</body>
</html>
```

The json2html template with id=4 is used to collect preferences:
```javascript
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sortable Criteria and Meta-alert table</title>
<style>
table {
    border-spacing: 0;
    width: 240%;
    border: 1px solid #ddd;
    table-layout: fixed;
}

th {
    cursor: pointer;
}

th, td {
    text-align: left;
    padding: 16px;
	word-wrap:break-word;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

td[Priority="1"]{
 background-color: #de1c24
}
td[Priority="2"]{
 background-color: #dc8c00
}
td[Priority="3"]{
 background-color: #f0c800
}
td[Priority="4"]{
 background-color: #3697dd
}
td[Priority="5"]{
 background-color: #b3fedf
}



div.target {
    width: 100%;
    height: 100px;
    margin: 0;
    padding: 0;
    overflow: auto;
}
div.source {
    width: 100%;
    height: 100px;
    margin: 0;
    padding: 0;
    overflow: auto;
}
</style>
</head>
<body onload="getContent(myObj)">

<h2>Criteria and Meta-alerts combined</h2>

<p id="demo"></p>

<script>
function sortTable(n) { //This function needs optimization - change of sorting algorithm
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("MAcriteria");
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

function download(content, fileName, contentType) {
    console.log("Content"+JSON.stringify(content)+"\n");
    var a = document.createElement("a");
    var file = new Blob([JSON.stringify(content)], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
	document.body.appendChild(a);
    a.click();
}

function downloadCriteria(content, fileName, contentType) {
    console.log("Content"+JSON.stringify(content)+"\n");
	var criteriafromcontent = [];
	for(var i = 0; i < content.length; i++) {
    var obj = content[i]['Criteria'];
	criteriafromcontent.push(obj);
    console.log(JSON.stringify(obj)+"\n");
    }
    var a = document.createElement("a");
    var file = new Blob([JSON.stringify(criteriafromcontent)], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
	document.body.appendChild(a);
    a.click();
}
myObj ={};
function getContent(myObj) {
var obj, dbParam, xmlhttp, myObj, x, txt = "";

refObjText= '{' + 
	'"Criteria": {' + 
	'"Priority": "",' +
//	'"ID": "",' + 
	'"SourceAssetCriticality": "",' + 
	'"TargetAssetCriticality": "",' +
	'"SeverityForAttackCategory": "",' + 
	'"MAQuality": "",' + 	
	'"TimeToDueDate": "",' + 
	'"TimeFromDetectTime": "",' + 
//	'"AttackSourceReputationScore": "",' + 
	'"MaxCVE": ""' + 
	'},' + 
	'"MetaAlert": {' + 
//	'"id": "",' + 
	'"detecttime": "",' + 
	'"category": "",' + 
//	'"topassetcriticality": "",' + 
	'"rule": "",' + 
//	'"maquality": "",' + 
	'"source_ip": "",' +
    '"source_port": "",' +
    '"source_type": "",' +
    '"source_maxassetcriticality": "",'+
    '"source_hostname": "",' +
    '"target_ip": "",' +
    '"target_port": "",' +
    '"target_type": "",' +
//    '"target_MaxAssetCriticality": "",' +
    '"target_hostname": "",' +
    '"protocol": "",' +
//    '"event": "",' +
//    '"Source_reputationscore": "",' +	
	'"rule": "",' +	
	'"id": "",' + 
	'"maxcvss": "",' + 
	'"aggrid": ""' + 
 '}}';
 
headersObjText= '{' + 
	'"Criteria": "Criteria",' + 
	'"Priority": "Priority",' +
	'"ID": "Id",' + 
	'"SourceAssetCriticality": "Source asset criticality",' + 
	'"TargetAssetCriticality": "Target asset criticality",' +
	'"SeverityForAttackCategory": "Severity for attack category",' + 
	'"MAQuality": "Quality",' + 	
	'"TimeToDueDate": "Time to due date",' + 
	'"TimeFromDetectTime": "Time from detection",' + 
	'"AttackSourceReputationScore": "Attack source reputation",' + 
	'"MaxCVE": "Max CVSS",' + 
	'"MetaAlert": "Meta-alert",' + 
	'"id": "Id",' + 
	'"detecttime": "Time of detection",' + 
	'"category": "Category",' + 
	'"topassetcriticality": "Top asset criticality",' + 
	'"rule": "Meta-alert correlation rule",' + 
	'"maquality": "Quality",' + 
	'"source_ip": "Source IP",' +
    '"source_port": "Source Port",' +
    '"source_type": "Source Type",' +
    '"source_maxassetcriticality": "Maximal source asset criticality",'+
    '"source_hostname": "Source hostname",' +
    '"target_ip": "Target IP",' +
    '"target_port": "Target port",' +
    '"target_type": "Target type",' +
    '"target_MaxAssetCriticality": "Maximal target asset criticality",' +
    '"target_hostname": "Target hostname",' +
    '"protocol": "Protocol",' +
    '"event": "Event",' +
    '"Source_reputationscore": "Source reputation",' +	
	'"rule": "Meta-alert correlation rule",' +	
	'"id": "Id",' + 
	'"aggrid": "IDs of correlated alerts"' + 
 '}';
 
refObj=JSON.parse(refObjText);
headersObj=JSON.parse(headersObjText);
formParam ="MinIndex=1&MaxIndex=3";
xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        myObj = JSON.parse(this.responseText);
		window.myObj=myObj;
		count=0;
		head="";
        for (x in myObj) {
		txt += "<tr>";
		count+=1;
		if (count ===1) {head+= "<tr>"; i=0}
		  for (y in refObj) {
		   for (z in refObj[y]){
		    if (count ===1){head+="<th onclick=\"sortTable("+i+")\">"+headersObj[z]+"</th>";i+=1;}
//			if (z === "Source"){
//					source="";
//					for (s in refObj[y][z]){
//						source+=s+":"+myObj[x][y][z][0][s]+"<br>";
//					}	
//					txt += "<td><div class=source>" + source + "</div></td>";
//				}
//				else {
//					if (z === "Target"){
//						target="";
						//tmpobj=JSON.parse(myObj[x][y][z]);
//						for (t in refObj[y][z]){
							//target+=t+":"+tmpobj[0][t]+"<br>";
//							target+=t+":"+myObj[x][y][z][0][t]+"<br>";
						//target+=myObj[x][y][z];
//					}	
//					txt += "<td><div class=target>" + target + "</div></td>";
//					}else{
					if (z === "Priority"){
//					txt += "<td "+z+"="+JSON.stringify(myObj[x][y][z])+">" + 
					txt += "<td nowrap "+z+"=NA>";
					if (myObj[x][y][z]==1)
					{
					 txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=1" value="1" checked="checked"> 1';
					}
					else {txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=1" value="1"> 1';}
					if (myObj[x][y][z]==2)
					{
					  txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=2" value="2" checked="checked"> 2';
					}
					else {txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=2" value="2"> 2'; 
					}
					if (myObj[x][y][z]==3)
					{
					  txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=3" value="3" checked="checked"> 3';
					}
					else{
					txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=3" value="3"> 3';
					}
					if (myObj[x][y][z]==4)
					{
					 txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=4" value="4" checked="checked"> 4'
					}
					else{
					txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=4" value="4"> 4';
					}
					if (myObj[x][y][z]==5)
					{
					 txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=5" value="5" checked="checked"> 5'
					}
					else{
					 txt+='<input type="radio" name="PriorityOf'+x+'" onclick="myObj['+x+']'+'[\''+y+'\'][\'Priority\']=5" value="5"> 5';
					}
//					JSON.stringify(myObj[x][y][z]) + "</td>";
					txt+="</td>";
					}
					else{
					txt += "<td>" + JSON.stringify(myObj[x][y][z]) + "</td>";
					}
//				}
//			}
			}
		  }
		txt += "</tr>";  
		if (count ===1) {head+= "</tr>";}
        }
        txt = "<form><table id='MAcriteria' >"+ head+txt;
		txt += "</table><br><input onclick=\"download(myObj,'LearningSet.txt', 'text/plain')\" type=\"button\" value=\"Download set in JSON\"><br><input onclick=\"downloadCriteria(myObj,'LearningSet.txt', 'text/plain')\" type=\"button\" value=\"Download criteria set in JSON\"></form>"        
        document.getElementById("demo").innerHTML = txt;
    }
};
xmlhttp.open("POST", "/rank-meta-alert", true);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send({{formParam}});
//xmlhttp.send();
}
</script>
</body>
</html>

```

## Custom Style Sheet for the /form interface
A visual appearance of the form available at http://{yourhost}:3001/form can be customised by modification of the template that is stored in *criteria-mapper* database in *form_css* table.
The default template is following:
```css
form {
  /* Just to center the form on the page */
  margin: 0 auto;
  width: 1000px;
  /* To see the outline of the form */
  padding: 1em;
  border: 1px solid #CCC;
  border-radius: 1em;
}

form div + div {
  margin-top: 1em;
}

label {
  /* To make sure that all labels have the same size and are properly aligned */
  display: inline-block;
  width: 200px;
  text-align: right;
}

input, textarea {
  /* To make sure that all text fields have the same font settings
     By default, textareas have a monospace font */
  font: 1em sans-serif;

  /* To give the same size to all text fields */
  width: 800px;
  box-sizing: border-box;

  /* To harmonize the look & feel of text field border */
  border: 1px solid #999;
}

input:focus, textarea:focus {
  /* To give a little highlight on active elements */
  border-color: #000;
}
h1 {
	font-family: Verdana;
	margin: 0 auto;
	width: 800px;
	text-align: center;
}
h2 {
	font-family: Verdana;
	margin: 0 auto;
	width: 800px;
}
p {
	margin: 0 auto;
	width: 800px;
}
textarea {
  /* To properly align multiline text fields with their labels */
  vertical-align: top;

  /* To give enough room to type some text */
  height: 5em;
}

.button {
  /* To position the buttons to the same position of the text fields */
  padding-left: 200px; /* same size as the label elements */
}

button {
  /* This extra margin represent roughly the same space as the space
     between the labels and their text fields */
  margin-left: .5em;
}
```

# EXAMPLES
## Input document from PostgreSQL database
```json
 {
      "id": "43187598-6984-4904-95a6-fcd9fa18a78b",
      "detecttime": "2018-11-08T16:16:22",
      "category": [
        "Attempt.Login",
        "MetaAlert"
      ],
      "maquality": 99,
      "rule": "TimeWindow_Events",
      "topassetcriticality": 0,
      "maxcve": 0.8,
      "aggrid": [
        "2ec92c32-c407-463d-bfa6-8223535dcd46"
      ],
      "source_ip": [],
      "source_port": [],
      "source_type": [],
      "source_maxassetcriticality": 0,
      "source_hostname": [
        "undefined"
      ],
      "target_ip": [
        "147.231.32.3"
      ],
      "target_port": [
        22
      ],
      "target_type": [],
      "target_maxassetcriticality": 0,
      "target_hostname": [
        "undefined"
      ],
      "protocol": [
        "ssh",
        "tcp",
        "undefined"
      ]
    }
```

## Output
Output (array of JSONs) produced by default (/) API endpoint of criteria-mapper for a document fetched from PostgreSQL Database (see Input... section), 
assuming usage of the template described above (see Template... section):
```json
[{
      "ID": "43187598-6984-4904-95a6-fcd9fa18a78b",
      "SourceAssetCriticality": "NA",
      "TargetAssetCriticality": "NA",
      "TimeToDueDate": "-172833.14253333333",
      "TimeFromDetectTime": "174333.14253333333",
      "SeverityForAttackCategory": "med",
      "MAQuality": "0.5",
      "AttackSourceReputationScore": "0.5",
      "MaxCVE": "0.8",
      "Priority": "5"
    }]
```
# Dockerisation
The module can be used also with Docker. To prepare Docker image issue 
the following commands:
```
git clone -b master-postgresql git@gitlab.com:protective-h2020-eu/meta-alert-prioritisation/criteria-mapper.git
```
in the directory cloned using git addjust parameters in criteria-mapper.js, and run:
```
criteria-mapper> npm install
criteria-mapper> docker.exe build -t criteria-mapper-app .
```

When the module is built, run it in the following way:
```
criteria-mapper>docker.exe run -p 3001:3001 --add-host="protective_postgresdb_1:172.17.0.x" --add-host="ranker:172.17.0.y" criteria-mapper-app
```
The IP address in --add-host parameter should correspond to the IP of dockerized version of PostgreSQL database and ranking-generator.

The web-service will be available at http://127.0.0.1:3001.

To stop the service use:
```
docker ps
//Note <CONTAINER ID> for the image
docker stop <CONTAINER ID>
```

# API <a href="API"></a>
Criteria-mapper modules provides following API endpoints (Note: ranking-generator module must be running and available for criteria-mapper to use most of endpoints (/criteria is only exepcion) and also database of meta-alerts must be accessible for the module):

Name | Method | Description
---|---|---
`/form` | `GET` | Does not required parameters and produce web-form that allows to test modules capabilities. The web-form uses CSS defined in *criteria-mapper* database in *form_css* table. The table has only two columns: *id* and *css*.
`/criteria` | `GET` | **Accepts following parameters:**  <br>`qID` (optional) - meta-alert identifier, <br>`minDetectTime` (optional)- minimal detection time (only more recent meta-alerts will be considered), <br>`maxDetectTime` (optional)- maximal detection time (only meta-alerts that occured before that date will be considered), <br>`category` (optional) - meta-alert category (e.g. Availability.DoS), <br>`source_ip` (optional) - IP of attack source, <br>`source_port` - source port of an attack, <br>`target_ip` (optional) - IP of a host being target of an attack, <br>`target_port` (optional) - port being attack target, <br>`limitSQL` (optional) - limit passed to SQL query (by default it is equal to 100000) to limit number of results passed to prioritisaiton module and to the browser, <br>`MinIndex` and `MaxIndex` (both optional) - can be used to fetched only slice of prioritised results (e.g. we can fetch  top 10 over 100000 meta-alerts when we do not specify limitSQL and set MinIndex=1&MaxIndex=10). <br>**Note:** the module uses also template with *id* equal to 3 as provided in *criteria-mapper* database -> *criteria_map* table, columns: *template* and *params*.  <br>**Provides results in the following JSON (that depends on mapping template):**<br> `[ {`<br>`    "TargetAssetCriticality": "NA",` <br>`    "SourceAssetCriticality": "NA",`<br>`    "TimeToDueDate": -170627.31625,`<br>`    "TimeFromDetectTime": 172127.31625,`<br>`    "MAQuality": 0.5,`<br>`    "AttackSourceReputationScore": 0.5,`<br>`    "SeverityForAttackCategory": "critical",`<br>`    "MaxCVE": 0,`<br>`    "ID": "c54e0c26-c5f9-47a1-a9e8-465645664cdb"`<br>`  },...]`
`/` | `GET` or `POST` | **Accepts following parameters:**  <br> All above mentioned (for /criteria) and the following parameters specific for ranking:<br>  `SortByPriority` (optional) - if is set to *on* then the results are sorted by priority (otherwise are retuned in the same order as provided by DBMS), <br>`user_id` (optional) - identifier of the user to be passed to ranking-generator, the default `user_id` is `protectvie`. <br>**Provides results in the following JSON:**<br>`    [  {`<br> `    "ID": "74aa825d-1338-4a5c-8623-dca480b71a30",`<br> `    "Priority": 5`<br> ` },`<br> ` {`<br> `    "ID": "f1f4fbc3-b3f3-4bc3-9995-6e17da40e53c",`<br> `    "Priority": 3`<br> ` },...`<br>`]` 
`/rank-ext` |  `GET` or `POST` |**Accepts following parameters:**  <br> All above mentioned (for /criteria).<br>**Note:** It uses the same mapping template (from *criteria_map* table) as /criteria endpoint.<br>**Provides results in the following JSON (that depends on mapping template):**<br>`[`<br>`  {`<br>`    "ID": "066692e7-756c-40b0-bb25-0a50a841bd1d",`<br>`    "SourceAssetCriticality": "NA",`<br>`    "TargetAssetCriticality": "NA",`<br>`    "TimeToDueDate": "-171275.42426666667",`<br>`    "TimeFromDetectTime": "172775.42426666667",`<br>`    "SeverityForAttackCategory": "critical",`<br>`    "MAQuality": "0.5",`<br>`    "AttackSourceReputationScore": "0.5",`<br>`    "MaxCVE": "0.0",`<br>`    "Priority": "5"`<br>`  },`<br>`  {`<br>`    "ID": "fe257d07-7bd8-48d6-b93a-efd98fd77eab",`<br>`    "SourceAssetCriticality": "NA",`<br>`    "TargetAssetCriticality": "NA",`<br>`    "TimeToDueDate": "-170560.5576",`<br>`    "TimeFromDetectTime": "172060.5576",`<br>`    "SeverityForAttackCategory": "critical",`<br>`    "MAQuality": "0.5",`<br>`    "AttackSourceReputationScore": "0.5",`<br>`    "MaxCVE": "0.0",`<br>`    "Priority": "5"`<br>`  },...]`
`/rank-meta-alert` |  `GET` or `POST` |**Accepts following parameters:**  <br> All above mentioned (for /) and additionally <br>`HtmlOut` (optional) - if it is set to *on* then results are returned in HTML form as provided in *json2html* template/column in *criteria-mapper* database -> *ranking_json2html* table (the template with *id* equal to 3 is used for that endpoint), otherwise JSON output is provided.<br> **Provides results in HTML or in the following JSON:**<br>`[`<br>`  {`<br>`    "Criteria": {`<br>`      "ID": "d6e7ba60-b55d-4f42-ba48-9892315d9703",`<br>`      "SourceAssetCriticality": "NA",`<br>`      "TargetAssetCriticality": "NA",`<br>`      "TimeToDueDate": "-171261.46638333335",`<br>`      "TimeFromDetectTime": "172761.46638333335",`<br>`      "SeverityForAttackCategory": "critical",`<br>`      "MAQuality": "0.5",`<br>`      "AttackSourceReputationScore": "0.5",`<br>`      "MaxCVE": "0.0",`<br>`      "Priority": "5"`<br>`    },`<br>`    "MetaAlert": {`<br>`      "id": "d6e7ba60-b55d-4f42-ba48-9892315d9703",`<br>`      "detecttime": "2018-11-09T21:09:13",`<br>`      "category": [`<br>`        "Availability.DoS",`<br>`        "MetaAlert"`<br>`      ],`<br>`      "maquality": 99,`<br>`      "rule": "TimeWindow_Events",`<br>`      "topassetcriticality": 0,`<br>`      "aggrid": [`<br>`        "e37d8e78-d8b0-4f8e-b8f8-28d05c6dc013"`<br>`      ],`<br>`      "source_ip": [],`<br>`      "source_port": [],`<br>`      "source_type": [],`<br>`      "source_maxassetcriticality": 0,`<br>`      "source_hostname": [`<br>`        "undefined"`<br>`      ],`<br>`      "target_ip": [`<br>`        "194.228.92.38"`<br>`      ],`<br>`      "target_port": [],`<br>`      "target_type": [],`<br>`      "target_maxassetcriticality": 0,`<br>`      "target_hostname": [`<br>`        "undefined"`<br>`      ],`<br>`      "protocol": [`<br>`        "undefined"`<br>`      ]`<br>`    }`<br>`  },...]`
`/` | `OPTIONS` | No parameters. Returns the following header: <br>'Content-Type': 'application/json','Access-Control-Request-Headers': 'Access-Control-Allow-Origin, Authorization','Access-Control-Allow-Headers': 'Access-Control-Allow-Origin, Authorization','Access-Control-Allow-Origin': '*'
`/get-learning-set` | `POST` | This enpoint is available in [pilot2-test-v3](https://gitlab.com/protective-h2020-eu/meta-alert-prioritisation/criteria-mapper/container_registry) docker image (and later).<br>**Accepts following parameters:**  <br> All above mentioned (for / (POST method)).<br>**Provides results in HTML:**<br>results are returned in HTML form as provided in *json2html* template/column in *criteria-mapper* database -> *ranking_json2html* table - the template with *id* equal to *4*  is used for that endpoint (**Note: it is different json to html conversion template than for /rank-meta-alert**).

**Note:** The **criteria_map** template (in criteria-mapper database) must be in sync with criteria definitions provided for **ranking-generator** and **rule-inducer** modules.


