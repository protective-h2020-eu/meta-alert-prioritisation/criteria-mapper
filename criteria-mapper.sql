PGDMP                         w            criteria-mapper    10.5    10.5                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            	           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            
           1262    16393    criteria-mapper    DATABASE     �   CREATE DATABASE "criteria-mapper" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Polish_Poland.1250' LC_CTYPE = 'Polish_Poland.1250';
 !   DROP DATABASE "criteria-mapper";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16396    criteria_map    TABLE     b   CREATE TABLE public.criteria_map (
    id integer NOT NULL,
    template text,
    params text
);
     DROP TABLE public.criteria_map;
       public         postgres    false    3            �            1259    16394    criteria_map_id_seq    SEQUENCE     �   CREATE SEQUENCE public.criteria_map_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.criteria_map_id_seq;
       public       postgres    false    197    3                       0    0    criteria_map_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.criteria_map_id_seq OWNED BY public.criteria_map.id;
            public       postgres    false    196            �            1259    16417    form_css    TABLE     H   CREATE TABLE public.form_css (
    id integer NOT NULL,
    css text
);
    DROP TABLE public.form_css;
       public         postgres    false    3            �            1259    16415    form_css_id_seq    SEQUENCE     �   CREATE SEQUENCE public.form_css_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.form_css_id_seq;
       public       postgres    false    201    3                       0    0    form_css_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.form_css_id_seq OWNED BY public.form_css.id;
            public       postgres    false    200            �            1259    16405    ranking_json2html    TABLE     W   CREATE TABLE public.ranking_json2html (
    id integer NOT NULL,
    json2html text
);
 %   DROP TABLE public.ranking_json2html;
       public         postgres    false    3            �            1259    16403    ranking_json2html_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ranking_json2html_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ranking_json2html_id_seq;
       public       postgres    false    3    199                       0    0    ranking_json2html_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.ranking_json2html_id_seq OWNED BY public.ranking_json2html.id;
            public       postgres    false    198            }
           2604    16399    criteria_map id    DEFAULT     r   ALTER TABLE ONLY public.criteria_map ALTER COLUMN id SET DEFAULT nextval('public.criteria_map_id_seq'::regclass);
 >   ALTER TABLE public.criteria_map ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    196    197    197            
           2604    16420    form_css id    DEFAULT     j   ALTER TABLE ONLY public.form_css ALTER COLUMN id SET DEFAULT nextval('public.form_css_id_seq'::regclass);
 :   ALTER TABLE public.form_css ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    201    200    201            ~
           2604    16408    ranking_json2html id    DEFAULT     |   ALTER TABLE ONLY public.ranking_json2html ALTER COLUMN id SET DEFAULT nextval('public.ranking_json2html_id_seq'::regclass);
 C   ALTER TABLE public.ranking_json2html ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    199    198    199                       0    16396    criteria_map 
   TABLE DATA                     public       postgres    false    197   �                 0    16417    form_css 
   TABLE DATA                     public       postgres    false    201                    0    16405    ranking_json2html 
   TABLE DATA                     public       postgres    false    199   #                  0    0    criteria_map_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.criteria_map_id_seq', 3, true);
            public       postgres    false    196                       0    0    form_css_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.form_css_id_seq', 1, true);
            public       postgres    false    200                       0    0    ranking_json2html_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.ranking_json2html_id_seq', 4, true);
            public       postgres    false    198            �
           2606    24683    criteria_map criteria_map_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.criteria_map
    ADD CONSTRAINT criteria_map_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.criteria_map DROP CONSTRAINT criteria_map_pkey;
       public         postgres    false    197            �
           2606    16425    form_css form_css_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.form_css
    ADD CONSTRAINT form_css_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.form_css DROP CONSTRAINT form_css_pkey;
       public         postgres    false    201            �
           2606    16413 (   ranking_json2html ranking_json2html_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.ranking_json2html
    ADD CONSTRAINT ranking_json2html_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.ranking_json2html DROP CONSTRAINT ranking_json2html_pkey;
       public         postgres    false    199                  x��VmO�0���
������B�(MU[�J�h��c�u�b-qJ��2���s�&�-m%شA����sw�}��s�m��P�w�Fa�a�$>�g���#t�8���"�����T~xxO1�B��QT�E`-�{�R�*�p��0q�p�;8hvȉ`�آ:��%f�Ā��Q�>�ϑ��EU��=4��Ut�bë�7��.<�W)�9�m��6�ҮkI�����YR�%%]/�	�M�k�#�up�{̥=��4%B���a?�Z�
J�t�?^���Ci�ԍؓ{���oT,�ʨ���������cv�	����p�=]'��M�&�V�nZ9a��I�t��I�2�,`CL~&���Q(��%�Ogu��������z6�V��PՐ�͸�XH(���¤�^15ȴp}�k��������'I7�С���<S��;����HSU ��H��r<&� YS&��Z��gh.�0���3��s�so�|lی,@��f�vn�O��0���2� g��3�SJ��.��C#-�y��� ��o=~�R�4��y�N��eZ��i�~l0�Ml�8��:6싇����-�rQ1�/2˖���q�4^T6�,-b��A�QV��~F���Q��OF�Z���ܮU��J�NJ�d�J)BH�5��婿��S�ƐJ��{Jg������ܺX��G3v>#/>fk�rs�8h���&����-��n�eJ�_�'�����P~w5���T����}&o�ly�g��>�;�lz�f����!��J.�������(�z��;,k&o\+q�s�XN(�,�1�P��/_��P�˥�@"+�w�g�M��Hg|���`+���x ������U�-�a�� �n�t�'�ټ2�P�E)-�,JI�E��Bc         �  x��V�N�H}E)H��1(����� ��)!yɮVm�l�h���60���S}�mfx��@_�v��i_����\��m�
�R��eZÏ�O�?~��'o�݄�����#!�ƠS!�cٸ�[V"]��*y��	��:#��w�sS��|>o�����ego�o�u|xL�Z��)�3k�J���e� Z
�������$V,���g�m�d��	�Wܠ�̤��#t���Q��|�û���)xY�R�j���]v9��?������6D^S��`)�?��f��S�ff�	��y 4������]m�lQ�]�e���%�l� o,nq*�-e���#��#_��&��am����y�Mq�V?l�i0O�CL	���ڲ';�u~~��f@-n���f��5*^,#�޴�y�Ң�����[Pp�D��ee�]6�a9����A-�[�KBy�����ߧ^�'�Y�V�(��~گŹ[��=�[k:̌�@�>��)��E�<����
$�ک�d<�\�5І�ܙ���`RȬ�[��z�M	#v#**��

ˌ=D�5�����< %��[��˙�G\��j�T9kѬW�yСY���c;R��m� ��������c��x߭?�d����tB�u�c:�B5ќ;Q2잛����^z<?���}DaG(������
���e�Y�DgY�(�bC� @t�v�P[�J�O�?��]?!��Ex2%����B�H��@>�����!��ޏ��X�_P�*�dAuR���vt�D�n�%Es��l�j���'���ɻ��`�a�G��uw(���P��<�p8xwyq�����9��[�m8��3j�G*�~�ҽ��w)�$q��@o6�#��Y{��iy�WVavK��K����<υ��=(�U_E�I������Xdw�t��z���E>�|
�:��_�C��¸�=�2B-`/��x��R\���4�6v�zc��`�.o         �  x��ks�8��+xڹ�S�b'��s�̤I���$�����$�-Ѷ6�����n��� �zZr�n�ͶRgb�A `�.�o���r����e�M��w�=y���؝)�Y�ף�w�Rm�I����7���]��:,��F��1N�>���x_{7������;8�#��c���3)��A.`H�Z��D��Z����
��2�g�x��Ku��KvD��4��Kڻ�?��A��$�c���0�'�5,�r�K��AǺ\�S5�>�|��i���N�� 8[r ٜ�]b�1�x]j���O��\Z ��G���c�����׵���OM˨���	�����s���I�a��b�q}��n����-ɀ7X[��|�����������ǻ�ϛ��y�{?�����>�3cབZ�^K���1Vı-�}m���+c��h(_�0G�|�;��i3Pu�%&�0���{-�|�3]~X�m�����!jI���Zé铰�f����rsf~����JoOq�b4���9��`m?PO�]�x�¯arP {R'f�,�dmSgn�S'��`:�����v���p��d�q�Z__�Ό�vq�+ah;8 �
qoΰ��"n�Bra>&�����l�� =0T�6M�}A.�#�X���aZ��LsF�Z�HۉM>�>1f�6�- X�`���,�hE|��*��C��q���܏(hd�����K�A�&SX&K�T����\!>6=�בR}*8��������w7 �x�j¤�b��"ʦ��	�i�@s�p��'��1�
 ��]5,�(I�{�R+�aR3|֎,(��.�	�z���� ��sfh�< FA�}v 6�^�(!K���6oc�Z���XU�h;��m@�*@j���A����w��,�eR��!�E�����.�Z]G�&�>����*\ix�8�o�4m�y���M�;���rHVy}q$b���,�Ɍzw@lE8JYxA,�nl`j���<b`԰VokXt�%C���Y�rV��"q,�M��
		���$���|@�Bzm��Y-|��}d�K�` 1<�+F�Uc�^�w��,��)�I����e��Q�:��G�Mtl��p��	���V�H�2$e�o����Ir���bc��W�%�/��	!zs��m.�N�h6�{.�����$y�P9 1G�yR��d�qS��a�}=�3����ztNxfM9wAAVo��2_���`Ҳ���:#�'��}�k[.U*Zyh]k�hմ:��[-�8;��$���{:;�A����!���PC��PD�8�/�w�9DXǔ3�B6�]�g���$��:'svC7���_�!�0k�_BH�伒��̝s t�ql��.�=���u�L�0���+�7�4m)��f�A":�6ɇ@��g31���L�Lc�`AH
�M�QBy�$%	���Ztv���v�9��lu<�����M˟Z:���ӛ��h2�T�l�^W*�E��{���x��ƀ� 72���o��6ز����{`���e�����k�z�~�3�\��tlp�ʇ���A��g���E����)���xM�h��s�:��q-,&�����nQ�T*	{���W��Z̞��y	^N�C��Z�����C���w$8� �6@�41��k S�W�Z�X���u\�nwTIaW���K�2������Y�vn�C�����Z|
$�H)� ����H`�L���S�B|��Q�oh^��5��Մ�����ڿ��GJV�K�7��q��J$�����TE��jG�tp��-��t��>�"�?�p�$Mxi�Do��ည:"��Մ��܃P���I�vj��w;�&=�'FU��OB(��0!nI�'[w�Zo��$vz�lk�J%J%���'� $����z��-/E!�/;�#��J�A�3\.���՛�P������,��@~1CD����l^�T���+�!꺰uEP�Z6�Emc��)]�0���l�z���z�@�Tʨ��̔HT�<h�Ϟ�S�<���~�S�r�e���T��>;i>e�d�E
[8v`ڡN�ˡ�8n�-g�%t�\�E��/8K� .�E��H	�"A��H	�"A\$��� .��Ez��`��܊�Z*R�E����w�5��ў����D��G��ϗMn�8lդ�����M<�D�Y"F��Y���Y#"�I�ǉR���l�EY䷋��_2��W䷋�v��.��E~��o��"�]��gE~��o��g��N$z���C����!(��m=7����<�|�{��e0pF��e��������1�r��T�8��u��2��4�4��M��'�LUW6��3E ثD��4u
U>�.�ဓ5�xV�Է�^�p~R�J�Y
f��|?��et2�Tk*7M�*|�HD�1�qs�G�Z�3�qfC��-$�.�2'�'o��f&��0�$܃y&(x�j����G��Lb��`�}��H�԰�T�� ��V�	�7r��`��0�"�؊@�:%�6i�p��d!nݎ3��c�蕷�G�Sb�&��,y��1��EL.�aT�yv��B*(Qk�f,�P$ o7a�������6�����ͬ�PivS�VA��L����n���|��η�j�l~�&�*xO@���T�$��m� {땞��%����On�R�>���ى�[0��K�^���(QFN"�tE%�\Ɍ����m�����첦�I6UKniS�?�����K�ۧ8%�QP�TX3˜��V�F�Rg��s;+�G<T�~�3�DE���){�?��g�({~e���)ä��YT?��gQ�,��E���~�O�_�4���"��� �`�-�f�NT�P���	�aִ�Ix��8W�qp���5����i�A�q��]U�q@$A%�^YΨz�3h�=u�qR�
mN=64�ޞ�� 6�Ձ�*��2@�����;�c�&��������#Eʧ*�I�"��/)��+�l�+) `��Zö�"�)%�~�r�	߫�� ��`^W*��Qf��t���
#�`⬤@��^���vl��4EeF���u	!�tW���U���*B�.m�L(�!��{�=���_�Bq��K_4X�6��M/������Aq��s_?؜�i6�/�VA�ˣ�s$ ����S4�T����z�G�Z�R�6�o���G����~�ԛ1��er��4�V���o�r}S����Wx��� ~���@�9��+�1���ܨ퐴т��T���I�$j�/�N ���;jɟ.�N�@'SΝ�!�8��(��*K�{y��;�Z����˔�^����B_��ʲ$��'����dF�C&�3�_�<y�I�=,�y�~����2d�e��_�����*(Kݩ+=�.������'��O���a�Z�(�Kh2+_��3������&`�T�p>�+_-ע�]�@L.��F�9w�`�n���Ā'`�(Q��o�9�4}^
��Ii	!~/7 [�D�#�D���2��     