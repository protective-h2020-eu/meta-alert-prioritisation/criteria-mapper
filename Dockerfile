FROM node:carbon


# Create app directory
WORKDIR /usr/src/app
RUN apt-get update 
RUN apt-get -y install postgresql-client
 

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 3001
CMD [ "./init-criteria-mapperDB.sh"]
CMD [ "npm", "start" ]

