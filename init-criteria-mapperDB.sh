#!/bin/bash
#echo "postgres" >.pgpass
if [  -z "${CLEARDB}" ]; then 
  echo 'Setting CLEARDB to true'
  CLEARDB=true
fi
  psql -h protective_postgresdb_1 -U postgres postgres < /usr/src/app/createDB.sql
if [ "${CLEARDB}" = true ]; then
  echo "CLEARDB is true"
  psql -h protective_postgresdb_1 -U postgres criteria-mapper < /usr/src/app/dropTables.sql
fi 
echo "Restoring original table content"
pg_restore -h protective_postgresdb_1 -U postgres -d criteria-mapper /usr/src/app/criteria-mapper.sql
