#!/bin/sh

if [ -z "${WAIT_FOR_HOST_AND_PORT}" ]; then
	echo "WAIT_FOR_HOST_AND_PORT not set using default value: protective_postgresdb_1:5432"
	WAIT_FOR_HOST_AND_PORT=protective_postgresdb_1:5432
fi

echo "Configuring criteria-mapper..."

# Wait for DB to be ready:
if [ -n "$WAIT_FOR_HOST_AND_PORT" ]; then
	HOST_AND_PORT=$WAIT_FOR_HOST_AND_PORT
	echo "Started waiting for $HOST_AND_PORT"

	/usr/src/app/wait-for-it.sh $HOST_AND_PORT -- /usr/src/app/init-criteria-mapperDB.sh
fi

